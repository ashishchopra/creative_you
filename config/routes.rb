Rails.application.routes.draw do
  # namespace :individual do
  # get 'portfolios/show'
  # end

  # namespace :individaul do
  # get 'portfolios/show'
  # end

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # devise_scope :user do
  #   get "/business", '/admin', '/individual', :to => "devise/sessions#new"
  # end


  devise_for :users do
    get 'users/sign_in' => 'devise/sessions#new'
    get '/users/sign_out' => 'devise/sessions#destroy'
  end

  namespace :business, path: :b do
    resources :users
    
    authenticated :user, lambda {|u| u.type == "Business"} do
      root :to => "home#index"
    end
  end

  namespace :individual, path: :i do
    resources :users do
      resources :connections
      resources :portfolios, only: [:index]
      get '/portfolio' => 'portfolios#show'
      post '/profile/update_display_information' => 'profile#update_display_information'
      put '/profile/update' =>'profile#update'
      get '/profile/edit' =>'profile#edit'
      get '/profile' =>'profile#show'
    end

    authenticated :user, lambda {|u| u.type == "Individual" ||  u.type == "ArtEnthusiast"} do
      root to: "home#index"
    end
  end

  namespace :admin do
    resources :users
    
    authenticated :user, lambda {|u| u.type == "Admin"} do
      root :to => "home#index"
    end
  end  
  
  root :to => 'home#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
