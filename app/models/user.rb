class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable
  validates :type, :category_id, :first_name, :last_name, :email, :account_id, presence: true

  belongs_to :account
  has_many :documents, as: :documentable, dependent: :destroy
  has_many :organizations, dependent: :destroy
  has_one :display_information, dependent: :destroy

  has_many :connections
  has_many :trainings
  has_many :contacts, through: :connections
  has_many :inverse_connections, class_name: "Connection", foreign_key: "contact_id"
  has_many :inverse_contacts, through: :inverse_connections, source: :user

  accepts_nested_attributes_for :organizations, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :display_information, reject_if: :all_blank, allow_destroy: true

  def full_name
  	"#{first_name} #{last_name}".titleize 
  end

  def parameterized_type
    return 'art_enthusiast' if type == 'ArtEnthusiast'
    return type.parameterize
  end

  def education_details
  	organizations.where(organization_type: 'school')
  end

  def experience
  	organizations.where(organization_type: 'company')
  end

  def display_picture
    display_information.profile_photo.url rescue nil
  end

  def display_title
    display_information.title rescue '-'
  end
end
