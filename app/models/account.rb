class Account < ActiveRecord::Base

	has_many :users
	validates :name, presence: true

	def self.default
		find_by(name: 'free')
	end

	def is_trail?
		return true if name == 'trial'
	end

	def is_free?
		return true if name == 'free'
	end

	def is_premium?
		return true if name == 'premium'
	end

end
