class Document < ActiveRecord::Base

belongs_to :documentable, polymorphic: true

has_attached_file :doc_file
validates_attachment :doc_file, presence: true


def documentable_type=(the_type)
	super(the_type.to_s.classify.constantize.base_class.to_s)
end

end
