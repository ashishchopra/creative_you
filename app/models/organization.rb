class Organization < ActiveRecord::Base

  has_many :users
  has_many :projects
  
  validates :organization_type, inclusion: {in: ['school', 'company']}
  validates :start_date, :end_date, :user_id, presence: true
  
end
