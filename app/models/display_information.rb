class DisplayInformation < ActiveRecord::Base
	self.table_name = 'display_information'
	
	has_attached_file :profile_photo
	validates_attachment :profile_photo, 
		content_type: { content_type: [/image/] }

	belongs_to :user
	
end
