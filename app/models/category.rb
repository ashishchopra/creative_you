class Category < ActiveRecord::Base

	def titleized_name
		name.titleize
	end
end
