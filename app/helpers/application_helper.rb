module ApplicationHelper

	def link_to_add_fields(name, f, association, org_type=nil)
    new_object = f.object.send(association).klass.new
    id = new_object.object_id
    fields = f.fields_for(association, new_object, child_index: id) do |builder|
      render(association.to_s.singularize + "_fields", f: builder, org_type: org_type )
    end

    link_to(name, '#', class: 'add_fields btn', data: { id: id, fields: fields.gsub('\n', '') })
  end

  def link_to_remove_fields(name, f)
  	link_to(name, '#', class: 'remove_fields')      
  end

end
