class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  layout :layout_by_resource

  before_filter :configure_permitted_parameters, if: :devise_controller?


  def layout_by_resource
  	if devise_controller? && [:new, :create].include?(action_name.to_sym)
      'home'
  	end
  end

  protected
  
  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up,) { |u| u.permit(:first_name, :last_name, :type, :dob, :phone, :category_id, :account_id, :email,
                    :current_password, :password, :password_confirmation) }
  end

  def after_sign_in_path_for(resource)
    return individual_root_path if resource.type.downcase == 'individual' || resource.type.downcase == 'artenthusiast'
    return business_root_path if resource.type.downcase == 'business'
    return admin_root_path if resource.type.downcase == 'admin'
    return root_path
  end

  def after_sign_out_path_for(resource_or_scope)
    root_path
  end

end
