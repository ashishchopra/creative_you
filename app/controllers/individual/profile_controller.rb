class Individual::ProfileController < ApplicationController
	layout 'individual'
	skip_before_action :verify_authenticity_token, only: [:update_display_information																																																																																																																																																																																									]

	def edit
		@display_information = current_user.display_information || current_user.build_display_information
	end

	def update
		current_user.update!(profile_params)
		redirect_to individual_user_profile_path(current_user)
	end

	def update_display_information
		dp = DisplayInformation.find_or_initialize_by(user_id: current_user.id)
		dp.profile_photo = profile_photo_params[:profile_photo].present? ? profile_photo_params[:profile_photo] : nil
		dp.save!

		render partial: '/layouts/common/upload_profile_photo'
	end

	private

	def profile_params
		params.require(current_user.parameterized_type.to_sym).permit(display_information_attributes: [:id, :title, :summary], 
			organizations_attributes: [:id, :name, :affiliation, :start_date, :end_date, :organization_type,
			:designation, :_destroy])
	end

	def profile_photo_params
		params.require(:display_information).permit(:profile_photo, :id)
	end

end