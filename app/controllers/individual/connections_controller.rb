class Individual::ConnectionsController < ApplicationController
	layout 'individual'

	def index
		@contacts = current_user.contacts
	end

end
