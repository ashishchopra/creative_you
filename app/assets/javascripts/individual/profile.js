// # Place all the behaviors and hooks related to the matching controller here.
// # All this logic will automatically be available in application.js.
// # You can use CoffeeScript in this file: http://coffeescript.org/

$(document).ready(function(){
	$('.profile_sub_block').hide();
	$(".profile_block").on('click', function(){
	  item_id = this.id
	  if($("#"+ item_id + " span").prop('class').indexOf('glyphicon-menu-down') > -1){
	    $("#"+ item_id + " span").addClass('glyphicon-menu-up')
	    $("#"+ item_id + " span").removeClass('glyphicon-menu-down')
	    $('.'+item_id).hide();
	  }
	  else{
	    $("#"+ item_id + " span").removeClass('glyphicon-menu-up')
	    $("#"+ item_id + " span").addClass('glyphicon-menu-down')
	    $('.'+item_id).show();
	  }
	});

	$('.edit_display_pic').on('click', function(){
		open_edit_profile();
	});

	// $('.display_pic .close_options').on('click', function(){
	$('body').on('click', '.display_pic .close_options', function(){
		close_edit_profile();
	});
	
	// $('.fileUpload #choose_display_pic').on('change', function(){ // Do not use. Binding issue with dom.
	$('body').on('change', '.display_pic .fileUpload #choose_display_pic', function(){
		fd = new FormData();
		fd.append('display_information[profile_photo]', $('#choose_display_pic')[0].files[0]);
		update_dp(fd);
	});

	$('body').on('click', '#display_information_update_form .remove_file', function(){
		fd = new FormData();
		fd.append('display_information[profile_photo]', '');
		update_dp(fd);
	});

}); 
////////////////////Document Ready ends here/////////////////////

open_edit_profile = function(){
	$('.display_pic .btn-options').css('display', 'block');
	$('.display_pic img').css('opacity', 0.5);
}

close_edit_profile = function(){
	$('.display_pic .btn-options').css('display', 'none');
	$('.display_pic img').css('opacity', 1);
}

update_dp = function(formdata){
	$.ajax({
		url: $('#display_information_update_path').val() + "?&authenticity_token=" + AUTH_TOKEN,
		type: 'POST',
    data: formdata,
    cache: false,
    dataType: 'Text',
    processData: false, // Don't process the files
    contentType: false, // Set content type to false as jQuery will tell the server its a query string request
		success: function(data){
			$('.display_pic').html(data)
			close_edit_profile();
		}
	})
}


