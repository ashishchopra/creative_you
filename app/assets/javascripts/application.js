// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//

//= require jquery-1.9.1
//= require jquery_ujs
//= require js/bootstrap
//= require_self

// $(window).load(function(){
//   $('.sidebar li a').click(function(e){
//     $('.sidebar li').removeClass('active');
//     var $parent = $(this).parent();
//     if (!$parent.hasClass('active')){
//       $parent.addClass('active');
//     }
//   });
// });

$(document).on('click', 'form .add_fields', function(event) {
  var regexp, time;
  time = new Date().getTime();
  regexp = new RegExp($(this).data('id'), 'g');
  $(this).before($(this).data('fields').replace(regexp, time));
  return event.preventDefault();
});

$(document).on('click', 'form .remove_fields', function(event){
  $(this).prev('input[type=hidden]').val('true');
  $(this).closest('.fields').hide();
  return event.preventDefault();
});

//// Mark navigated link as active
$(window).load(function(){
  $('body').on('click', '.sidebar li a', function(){
    $('.sidebar li').removeClass('active');
    $(this).parent('li').addClass('active');
  });
});
