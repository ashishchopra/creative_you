class CreatePortfolios < ActiveRecord::Migration
  def change
    create_table :portfolios do |t|

      t.string :title
      t.string :summary
      t.attachment :profile_photo
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
