class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
    	t.string :name
    	t.string :url
    	t.date :start_date
    	t.date :end_date
    	t.string :description
    	t.integer :user_id
    	t.integer :organization_id

      t.timestamps null: false
    end
  end
end
