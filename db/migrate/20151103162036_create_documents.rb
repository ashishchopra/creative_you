class CreateDocuments < ActiveRecord::Migration
  def change
    create_table :documents do |t|

      t.integer :documentable_id
      t.string :documentable_type
      t.string :document_type
      t.attachment :doc_file

      t.timestamps null: false
    end
  end
end
