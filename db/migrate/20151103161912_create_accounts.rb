class CreateAccounts < ActiveRecord::Migration
  def change
    create_table :accounts do |t|

      t.string :name#, :enum, limit: [:free, :premium, :trial], default: :free
      t.timestamps null: false
    end
  end
end
