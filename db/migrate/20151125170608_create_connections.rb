class CreateConnections < ActiveRecord::Migration
  def change
    create_table :connections do |t|
    	t.integer :user_id
    	t.integer :contact_id

      t.timestamps null: false
    end

    add_column :users, :contact_id, :integer
  end
end
