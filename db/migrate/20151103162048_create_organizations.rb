class CreateOrganizations < ActiveRecord::Migration
  def change
    create_table :organizations do |t|

      t.string :name
      t.string :organization_type
      t.string :affiliation
      t.date :start_date
      t.date :end_date
      t.string :designation
      t.integer :user_id
      t.timestamps null: false
    end
  end
end
