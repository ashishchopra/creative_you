namespace :demo_setup do
  task :run => :environment do
  	puts "scrapping records..."
  	# system('sudo apt-get install imagemagick -y')
  	ActiveRecord::Base.connection.execute("truncate table users")
  	ActiveRecord::Base.connection.execute("truncate table categories")
  	ActiveRecord::Base.connection.execute("truncate table accounts")
  	puts 'creating new records...'
    system("rake db:fixtures:load FIXTURES=accounts")
    system("rake db:fixtures:load FIXTURES=categories")
    system("rake db:fixtures:load FIXTURES=users")
    puts 'Done.'
  end
end
